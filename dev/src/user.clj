(ns ^{:doc "Development namespace"
      :author "Sławek Gwizdowski"}
  user
  (:require [szew.essbase.otl :as ess.otl
             :refer [member-seq extract-dimensions]]
            [szew.io :refer [in! xml]]
            [clojure.pprint :refer [pprint print-table]]
            [clojure.repl :refer :all]))

(def otl "datasets/sample_basic.xml")

(def dims (in! (xml {:processor (comp vec extract-dimensions)}) otl))

(defn freqr [d]
  [(:name d)
   (into (sorted-map) (frequencies (mapcat keys (drop 1 (member-seq d)))))])

(def p (first (filter (comp (partial = "100-30") :name)
                      (member-seq {:members dims}))))

(def m (first (filter (comp (partial = "COGS") :name)
                      (member-seq {:members dims}))))

(def a (first (filter (comp (partial = "Intro Date") :name)
                      (member-seq {:members dims}))))


