# xmlotlext

Command line utility to extract dimensions from Essbase XML outline export.

Friends call it `xoe`.

## Usage

Per `--help`:

```sh
xmlotlext - Extract dimensions from Essbase XML outline export.

Examples:

* List dimensions to STDOUT (w/o header):
  --list --source OTL.xml

* Produce triples: [Dimension, Property, Value] to TARGET (w/ header):
  --inspect --source OTL.xml --target Dimensions.csv

* Extract dimension into generational file given as TARGET:
  --extract Measures --format gen --source OTL.xml --target Measures.csv

* Produce triples: [Path, Property, Value] to TARGET (w/ header):
  --extract Measures --format flat --source OTL.xml --target Measures.csv

* Produce relational exports of all dimensions into TARGET-DIR:
  --dump --source OTL.xml --target-dir /tmp/x/ --format relational

Interface:

  -l, --list                          List dimensions available in source XML.
  -i, --inspect                       List dimension properties from source XML.
  -e, --extract DIMENSION             Extract single dimension from source XML.
  -d, --dump                          Extract all dimensions from source XML to target dir.
  -f, --format NAME          gen      One of: gen, flat, eflat, relational.
  -j, --delimiter CHARACTER  ,        Output delimiter character.
  -z, --eol EOL              dos      Output end of line, either unix or dos.
      --gen-prefix STRING    GEN      Generation prefix for 'gen' export.
      --alias-prefix STRING  ALIAS    Alias prefix for 'gen' export.
      --force-quote                   Quote every field, escape quotes.
  -a, --alias-table STRING   Default  Alias table to read.
  -s, --source PATH                   Source XML Outline extract.
  -t, --target PATH                   Output file, skip for STDOUT.
  -T, --target-dir PATH      .        Directory for --dump exports.
      --print-state                   Print program state and exit.
      --license                       Print legal information and exit.
  -v, --version                       Print version information and exit.
  -h, --help                          Print usage and exit.
```

See `helpers/` directory for sample invocation helpers.

## Development

Uses [Leiningen][lein] and public libraries. To start:

1. Install `lein`
2. Clone the repository
3. Enter the repository
4. Run `lein test`.

This should fetch the dependencies and run the test suite.

### Build

While in repository `lein uberjar` will produce `xoe.jar` in target directory.

From that on you only need to call it:

```sh
java -jar xoe.jar --list --source OTL.xml
```

## License

Software shared under [MIT License][license], copyright (c) Sławek Gwizdowski.

[lein]: https://leiningen.org/
[license]: LICENSE
