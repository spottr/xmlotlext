@echo off
SET PATH="Path\to\java\jdkx.x_xxx\bin";%PATH%
SET MEM=-server -Xms128m -Xmx1G
SET OPT1=-XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+ScavengeBeforeFullGC -XX:+CMSScavengeBeforeRemark
SET OPT2=-XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=90 -XX:-OmitStackTraceInFastThrow
SET JAR=xoe.jar

SET EXTRACT=--extract Measures
SET SOURCE=--source datasets/sample_basic.xml
SET TARGET=--target datasets/Sample_Basic_Measures_gen.csv

java %MEM% %OPT1% %OPT2% -jar %JAR% %EXTRACT% %SOURCE% %TARGET%

