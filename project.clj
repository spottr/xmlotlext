(defproject xmlotlext "0.2.2"
  :description "Extract dimensions from Essbase XML outline export."
  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory & headers."}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/tools.cli "1.0.206"]
                 [szew/essbase "0.3.4"]
                 [szew/io "0.5.3"]
                 [environ "1.2.0"]]
  :profiles {:dev {:dependencies [[criterium "0.4.6"]
                                  [hashp "0.2.1"]]
                   :plugins [[lein-environ "1.2.0"]]
                   :source-paths ["dev/src"]
                   :env {:dev "TRUE"}
                   :java-opts ["--illegal-access=deny"]}
             :uberjar {:aot :all
                       :env {:prod "TRUE"}}}
  :plugins []
  :main xmlotlext.core
  :repl-options {:init-ns user}
  :uberjar-name "xoe.jar"
  :aot [])
