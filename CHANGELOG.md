# Change Log

All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## Wishlist

- Nothing at the moment.
## [Unreleased]

- ...

## [0.2.2] - 2021-09-22

### Fixed

- Very silly `name` vs `:name` error in gen export.

## [0.2.1] - 2021-09-07

### Added

- Added MemberFormula to relational export.

## [0.2.0] - 2021-09-07
### Fixed

- Removed space in between group and name in Property column in flat.
- Upgraded dependency to enable formula extraction in 11.2.
- Dimension roots not included in Dimension exports, Members only.

### Added

- Implemented "relational" export, which only has fixed member
  columns: Parent, Name, Alias, Data Storage, Consolidation, Shared,
  Time Balance, Two Pass, Variance Reporting, UDAs, [Sorted Attributes]*

### Changed

- Full cube --dump only does single format now, gen still default.
- Inspect call is no longer part of --dump.

## [0.1.1] - 2021-08-30

### Fixed

- NullPointerException in --dump remediated.
- Several linter warnings removed.
- Streamlined docs.

### Added

- Send output to STDOUT if --target is skipped.
- Refreshed dependencies.

## [0.1.0] - 2019-06-27

- Initial release.

[Unreleased]: https://bitbucket.org/spottr/xmlotlext/branches/compare/master%0D0.2.2#diff
[0.2.2]: https://bitbucket.org/spottr/xmlotlext/branches/compare/0.2.2%0D0.2.1#diff
[0.2.1]: https://bitbucket.org/spottr/xmlotlext/branches/compare/0.2.1%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/xmlotlext/branches/compare/0.2.0%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/xmlotlext/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/xmlotlext/branches/compare/0.1.0%0Dafba07d1e3df3fe9b12919d83e0164f83d5005bf#diff
