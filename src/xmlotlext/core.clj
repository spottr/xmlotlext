; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "Main application entry point."
      :author "Sławek Gwizdowski"}
  xmlotlext.core
  (:require [xmlotlext.cli
             :refer [validate error-message usage-message license]]
            [xmlotlext.cmd.list :as cmd.list]
            [xmlotlext.cmd.inspect :as cmd.inspect]
            [xmlotlext.cmd.extract :as cmd.extract]
            [xmlotlext.cmd.dump :as cmd.dump]
            [clojure.java.io :refer [input-stream resource]]
            [clojure.pprint :refer [pprint]])
  (:import [java.util Properties])
  (:gen-class))

(defn get-version
  "Try to get requested dependency and return its version.
  "
  [dep]
  (let [path (str "META-INF/maven/" (or (namespace dep) (name dep))
                  "/" (name dep) "/pom.properties")
        props (resource path)]
    (when props
      (with-open [stream (input-stream props)]
        (let [props (doto (Properties.) (.load stream))]
          (.getProperty props "version"))))))


(defn -main
  "Entry point of the app."
  [& args]
  (let [{:keys [options arguments errors summary _] :as _}
        (validate args)
        result (cond
                 (:print-state options)
                 (pprint {:options (into (sorted-map) options)
                          :arguments arguments
                          :errors errors})
                 (seq errors)
                 (do (println (error-message errors)) 1)
                 (:help options)
                 (println (usage-message summary))
                 (:license options)
                 (println license)
                 (:version options)
                 (println (format "xmlotlext %s" (get-version 'xmlotlext)))
                 (:list options)
                 (cmd.list/run options)
                 (:inspect options)
                 (cmd.inspect/run options)
                 (:extract options)
                 (cmd.extract/run options)
                 (:dump options)
                 (cmd.dump/run options)
                 :else
                 (println (usage-message summary)))]
    (System/exit (or result 0))))
