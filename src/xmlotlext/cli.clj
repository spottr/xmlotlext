; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "CLI parsing and validation."
      :author "Sławek Gwizdowski"}
  xmlotlext.cli
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :refer [as-file]]
            [clojure.string :refer [lower-case join]]
            [szew.io.util :refer [all any]]
            [szew.io.fu
             :refer [exists? not-exists? file? directory? readable? writable?
                     parent-path]]
            [szew.essbase.otl :refer [list-dimensions]]))

(def ^{:doc "License text to be printed via CLI --license."}
  license
"
Copyright (c) 2017-2021 Sławek Gwizdowski

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the \"Software\"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
")

(def ^{:doc "Header and examples for usage message."}
  usage
"
xmlotlext - Extract dimensions from Essbase XML outline export.

Examples:

* List dimensions to STDOUT (w/o header):
  --list --source OTL.xml

* Produce triples: [Dimension, Property, Value] to TARGET (w/ header):
  --inspect --source OTL.xml --target Dimensions.csv

* Extract dimension into generational file given as TARGET:
  --extract Measures --format gen --source OTL.xml --target Measures.csv

* Produce triples: [Path, Property, Value] to TARGET (w/ header):
  --extract Measures --format flat --source OTL.xml --target Measures.csv

* Produce relational exports of all dimensions into TARGET-DIR:
  --dump --source OTL.xml --target-dir /tmp/x/ --format relational
")

(defn usage-message
  "Produce usage instructions message.
  "
  [summary]
  (str usage \newline
       (when (seq summary)
         (str "Interface:" \newline \newline summary))))

(defn error-message
  "Produce error message.
  "
  [errors]
  (when (seq errors)
    (str "There were errors:" \newline
         (join \newline (mapv (partial str "  ") errors)) \newline \newline
         "Please check --help for interface information.")))

(def ^{:doc "Command Line Interface"}
  options
  [["-l" "--list" "List dimensions available in source XML."
    :default false]
   ["-i" "--inspect" "List dimension properties from source XML."
    :default false]
   ["-e" "--extract DIMENSION" "Extract single dimension from source XML."
    ]
   ["-d" "--dump" "Extract all dimensions from source XML to target dir."
    :default false]
   ["-f" "--format NAME" "One of: gen, flat, eflat, relational."
    :default "gen"
    :parse-fn (comp lower-case str)
    :validate [#{"gen" "flat" "eflat" "relational"}
               "Format must be one of: gen, flat, eflat, relational."]]
   ["-j" "--delimiter CHARACTER" "Output delimiter character."
    :default \,
    :parse-fn (comp first str)
    :validate [(complement nil?) "Delimiter cannot be empty!"]]
   ["-z" "--eol EOL" "Output end of line, either unix or dos."
    :default "dos"
    :parse-fn (comp lower-case str)
    :validate [#{"unix" "dos"}
               "End of line must be either unix or dos."]]
   [nil "--gen-prefix STRING" "Generation prefix for 'gen' export."
    :default "GEN"]
   [nil "--alias-prefix STRING" "Alias prefix for 'gen' export."
    :default "ALIAS"]
   [nil "--force-quote" "Quote every field, escape quotes."
    :default false]
   ["-a" "--alias-table STRING" "Alias table to read."
    :default "Default"]
   ["-s" "--source PATH" "Source XML Outline extract."
    :parse-fn (comp as-file str)
    :validate [(all exists? file? readable?)
               "Source file not found or not readable!"]]
   ["-t" "--target PATH" "Output file, skip for STDOUT."
    :parse-fn (fnil (comp as-file str) nil)
    :validate [(any nil?
                    (all exists? file? writable?)
                    (all not-exists? (comp writable? parent-path)))
               "Cannot write to or create target file!"]]
   ["-T" "--target-dir PATH" "Directory for --dump exports."
    :default "."
    :parse-fn (comp as-file str)
    :validate [(all exists? directory? writable?)
               "Cannot write to or create target directory!"]]
   [nil "--print-state" "Print program state and exit."
    :default false]
   [nil "--license" "Print legal information and exit."
    :default false]
   ["-v" "--version" "Print version information and exit."]
   ["-h" "--help" "Print usage and exit."
    :default false]])

(defn validate-list
  "Validate options for --list command.
  "
  [{:keys [options arguments errors summary license] :as bundle}]
  (if-not (:list options)
    bundle
    (cond-> bundle
      (nil? (:source options))
      (update :errors (fnil conj [])
              "Failed to validate \"--list \": No valid \"--source\"!"))))

(defn validate-inspect
  "Validate options for --inspect command.
  "
  [{:keys [options arguments errors summary license] :as bundle}]
  (if-not (:inspect options)
    bundle
    (cond-> bundle
      (nil? (:source options))
      (update :errors (fnil conj [])
              "Failed to validate \"--inspect \": No valid \"--source\"!"))))

(defn validate-extract
  "Validate options for --extract command.
  "
  [{:keys [options arguments errors summary license] :as bundle}]
  (let [dim (comp first
                  (partial filter (comp (partial = (:extract options)) :name))
                  list-dimensions)]
    (if-not (:extract options)
      bundle
      (cond-> bundle
        (nil? (:source options))
        (update :errors (fnil conj [])
                "Failed to validate \"--extract \": No valid \"--source\"!")))))

(defn validate-dump
  "Validate options for --dump command.
  "
  [{:keys [options arguments errors summary license] :as bundle}]
  (if-not (:dump options)
    bundle
    (cond-> bundle
      (nil? (:source options))
      (update :errors (fnil conj [])
              "Failed to validate \"--dump \": No valid \"--source\"!")
      (nil? (:target-dir options))
      (update :errors (fnil conj [])
              "Failed to validate \"--dump \": No valid \"--target-dir\"!"))))

(defn validate
  "Parse arguments and validate option combinations.
  "
  [args]
  (let [{:keys [options arguments errors summary license] :as bundle}
        (-> (parse-opts args options)
            (assoc :license license))]
    (-> bundle
        validate-list
        validate-inspect
        validate-extract
        validate-dump)))
