; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "Command namespace for --dump."
      :author "Sławek Gwizdowski"}
  xmlotlext.cmd.dump
  (:require [clojure.java.io :as clj.io]
            [xmlotlext.cmd.extract :as cmd.extract]
            [xmlotlext.cmd.core :refer [cached]])
  (:import [java.io File]))

;; 1. Get basename of the file in --source, say NAME.xml
;; 2. Put extract into TARGET_DIR + NAME + _dimensions.txt
;; 3. Find all dimension names
;; 3.1. Extract flat into TARGET_DIR + NAME + DIM_NAME + _flat.txt
;; 3.1. Extract gen into TARGET_DIR + NAME + DIM_NAME + _gen.txt
;; Do all this by flipping options and reusing sibling modules.
;; Report progress as you go, but don't bail on failures?

(defn target-name-base
  "Base name for exports: remove file extension of source if exists.
  "
  [source]
  (let [src-name (.getName (clj.io/as-file source))
        dot-idx  (.lastIndexOf ^String src-name ".")]
    (if (pos? dot-idx)
      (.substring ^String src-name 0 dot-idx)
      src-name)))

(defn run-extracts
  "Execute requested format exports on all dimensions in the export."
  [{:keys [target-dir source] :as options}]
  (let [status     (volatile! 0)]
    (doseq [dim (mapv :name (cached source))]
      (let [ofile  (File. ^File target-dir
                          ^String (format "%s_%s_%s.txt"
                                          (target-name-base source)
                                          dim
                                          (:format options "unk")))
            opts   (assoc options :extract dim :target ofile)]
        (when-not (zero? (cmd.extract/run opts))
          (vreset! status 1))))
    @status))

(defn run
  "Execute the --dump command, print to stdout.
  "
  [{:keys [source target-dir] :as options}]
  (try
    (when-not target-dir
      (throw (ex-info "Requires valid --target-dir"
                      {:target-dir target-dir})))
    (when-not source
      (throw (ex-info "Requires valid --source" {:source source})))
    (run-extracts options)
    (catch Exception ex
      (println "There were errors:")
      (println ex)
      1)))
