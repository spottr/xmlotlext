; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "Command namespace for --list"
      :author "Sławek Gwizdowski"}
  xmlotlext.cmd.list
  (:require [clojure.string :refer [join]]
            [xmlotlext.cmd.core :refer [cached]]))

(defn run
  "Execute the --list command, print to stdout."
  [{:keys [source eol]}]
  (try
    (when-not source
      (throw (ex-info "Requires valid --source" {:source source})))
    (let [nl ({"dos" "\r\n" "unix" "\n"} eol)]
      (print (str (join nl (mapv :name (cached source))) nl))
      (.flush *out*))
    0
    (catch Exception ex
      (println "There were errors:")
      (println ex)
      1)))
