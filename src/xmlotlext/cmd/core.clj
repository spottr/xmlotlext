(ns xmlotlext.cmd.core
  (:require
   [szew.essbase.otl :refer [extract-dimensions!]]))

(def cache (atom nil))

(defn cached [source]
  (if (nil? @cache)
    (reset! cache (extract-dimensions! source))
    @cache))
