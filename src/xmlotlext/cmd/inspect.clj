; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "Command namespace for --inspect."
      :author "Sławek Gwizdowski"}
  xmlotlext.cmd.inspect
  (:require [szew.io :refer [sink csv]]
            [szew.essbase.otl :refer [member-seq]]
            [clojure-csv.core :as csv]
            [xmlotlext.cmd.core :refer [cached]]))

(defn inspector
  "Add additional properties to dimension, remove members, debug info etc.

  This does a full tally of members in a dimension, so reads the whole thing."
  [dim]
  (let [l0c (count (filter :level0? (member-seq dim)))
        aka (->> (member-seq dim)
                 (mapcat (comp (fnil keys []) :aliases))
                 (into (hash-set)))]
    (-> dim
        (assoc :level0-count l0c)
        (assoc :alias-tables aka)
        (dissoc :members :contained :tag))))

(defn present
  "Turn dimension into vector of triples [Dimension, Property, Value]."
  [inspected]
  (let [unroll     (fn [[k v]]
                     (if (map? v)
                       (mapv #(vector (str k ":" (first %)) (second %)) v)
                       [[k v]]))
        empty!?    (fn [x] (try (empty? x) (catch Exception _ false)))
        properties (->> (assoc inspected :misc nil)
                        (filterv (comp not empty!? second))
                        (mapv (juxt (comp name first) second))
                        (mapcat unroll)
                        (sort-by first)
                        vec)
        best-value (fn [[_ v :as _]]
                     (cond (string? v)
                           v
                           (and (or (seq? v) (vector? v) (set? v))
                                (empty? v))
                           ""
                           (or (seq? v) (vector? v) (set? v))
                           (csv/write-csv [(vec (sort (map str v)))]
                                          :end-of-line "")
                           :else (str v)))]
    (mapv (juxt (constantly (:name inspected)) first best-value) properties)))

(defn run
  "Execute the --inspect command - print to stdout."
  [{:keys [source target delimiter eol force-quote] :as _}]
  (try
    (when-not source
      (throw (ex-info "Requires valid --source" {:source source})))
    (let [output (sink (csv {:delimiter delimiter
                             :eol ({"dos" "\r\n" "unix" "\n"} eol "\r\n")
                             :force-quote force-quote})
                       (if (nil? target) *out* target))
          head ["Dimension" "Property" "Value"]]
      (output (cons head (mapcat present (mapv inspector (cached source)))))
      0)
    (catch Exception ex
      (println "There were errors:")
      (println ex)
      1)))
