; Copyright (c) Sławek Gwizdowski

; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:

; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.

; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
(ns ^{:doc "Command namespace for --extract."
      :author "Sławek Gwizdowski"}
  xmlotlext.cmd.extract
  (:require [szew.io :refer [sink csv]]
            [szew.io.util :refer [row-adjuster]]
            [szew.essbase.otl
             :refer [member-seq]]
            [clojure-csv.core :as csv]
            [xmlotlext.cmd.core :refer [cached]]))

;; gen

(defn path-to-alias
  "Return Generations including this members and alias of this members."
  [alias-table member]
  [(conj (:path member) (:name member))
   (get-in member [:aliases alias-table] "")])

(defn splice-aliases
  "Get Generations, return GenX,AliasX,GenX+1,AliasX+1."
  [m-o-a gens]
  (when-not (pos? (count gens))
    (throw (ex-info "Empty gens?" {:gens gens})))
  (let [sub-gens (mapv #(subvec %1 0 %2)
                       (repeat (vec gens))
                       (range 1 (inc (count gens))))]
    (vec (mapcat (juxt peek #(get m-o-a % "")) sub-gens))))

(defn gen-max
  "Get max generation number from map-of-aliases."
  [m-o-a]
  (reduce max 0 (map count (keys m-o-a))))

(defn gen-header
  "Generate header for given dimension name and number of generations."
  [gen-prefix alias-prefix counter dimension alias-table]
  (letfn [(gen-head [counter]
            (format "%s%d:%s" gen-prefix counter dimension))
          (alias-head [counter]
            (format "%s%d:%s" alias-prefix counter alias-table))]
    (->> (range counter)
         (mapcat (juxt gen-head alias-head))
         vec)))

(defn gen-extract
  "Prepare and write generation extract to target."
  [{:keys [source target extract alias-table gen-prefix alias-prefix
           delimiter eol force-quote]
    :as _}]
  (let [m-o-a (->> (cached source)
                   (filter (comp (partial = extract) :name))
                   first
                   member-seq
                   (map (partial path-to-alias alias-table))
                   (into (hash-map)))]
    (when (empty? m-o-a)
      (throw (ex-info "Dimension not present or empty?"
                      {:dimension extract :source source})))
    (let [gens   (gen-max m-o-a)
          header (gen-header gen-prefix alias-prefix gens extract alias-table)
          d-row  (mapv (constantly "") (range (* 2 gens)))
          output (sink (csv {:delimiter delimiter
                             :eol ({"dos" "\r\n" "unix" "\n"} eol)
                             :force-quote force-quote})
                       (if (nil? target) *out* target))]
      (output
        (cons header
              (sort-by (partial apply str)
                       (mapv (comp (row-adjuster d-row)
                                   (partial splice-aliases m-o-a))
                             (keys m-o-a))))))))

;; flat & eflat

(defn flat-member
  "Remove and flatten member to prepare for flat output."
  [member]
  (let [path+      (csv/write-csv [(mapv str (:path member []))]
                                  :end-of-line "" :delimiter \/)
        sanitized  (assoc member :members nil
                          :tag nil
                          :contained nil
                          :alias nil
                          :misc nil
                          :path path+)
        unroll     (fn [[k v]]
                     (if (map? v)
                       (mapv #(vector (str k ":" (first %)) (second %)) v)
                       [[k v]]))
        empty!?    (fn [x] (try (empty? x) (catch Exception _ false)))
        properties (->> sanitized
                        (filterv (comp not empty!? second))
                        (mapv (juxt (comp name first) second))
                        (mapcat unroll)
                        (sort-by first)
                        vec)
        best-value (fn [[_ v :as _]]
                     (cond (string? v)
                           v
                           (and (or (seq? v) (vector? v) (set? v))
                                (empty? v))
                           ""
                           (or (seq? v) (vector? v) (set? v))
                           (csv/write-csv [(vec (sort (map str v)))]
                                          :end-of-line "")
                           :else (str v)))]
  ;; drop debug data, like :tag, :contained, :path, :alias, :members
  ;; squash aliases into separate fields, with key containing alias table
  ;; squash attributes into new structure fields, key containing attr dim
  ;; keep :name, :parent, :consolidation, :data-storage, :dimension
  ;;       :generation, :level :shared-member, :two-pass-calc etc.
  ;; squash UDAs into a single field?
    (mapv (juxt first best-value) properties)))

(defn flat-path
  "Remove and flatten member to prepare for flat output.
  "
  [member]
  (let [path (csv/write-csv [(mapv str (conj (vec (:path member))
                                                (:name member)))]
                            :end-of-line "" :delimiter \/)]
    (mapv (partial into [path]) (flat-member member))))

(defn dim-id-flattener
  "Create a counting identifier flat-dim-id, giving 'dim-name#num'."
  [dim-name]
  (let [id (volatile! 0)]
    (fn flat-dim-id
      [member]
      (let [did (str (or dim-name (:dimension member (:name member)))
                     ":" @id)]
        (vswap! id inc)
        (mapv (partial into [did]) (flat-member member))))))

(defn flat-extract
  "Extract dimension into a flat CSV file."
  [{:keys [source target extract delimiter eol force-quote format] :as _}]
  (let [mbrs (->> (cached source)
                  (filter (comp (partial = extract) :name))
                  first
                  member-seq
                  (drop 1)
                  vec)]
    (when (= 0 (count mbrs))
      (throw (ex-info "Dimension not present or empty?"
                      {:dimension extract :source source})))
    (let [output (sink (csv {:delimiter delimiter
                             :eol ({"dos" "\r\n" "unix" "\n"} eol)
                             :force-quote force-quote})
                       (if (nil? target) *out* target))]
      (condp = format
        "flat"
        (output (cons ["Path" "Property" "Value"]
                      (mapcat flat-path mbrs)))
        "eflat"
        (output (cons ["Entity" "Property" "Value"]
                      (mapcat (dim-id-flattener nil) mbrs)))))))

;; relational

(defn generate-attribute-keys
  "Walk members, return sorted vector of attributes present."
  [members]
  (vec (sort (distinct (mapcat (comp keys :attributes) members)))))

(defn member-row
  "Turn member into a row with given fixed columns upfront and sorted
  by attribute name attributes in tail."
  [alias-table attribute-keys member]
  (mapv str
        (into [(get member :parent)
               (get member :name)
               (get-in member [:aliases alias-table] "")
               (get member :data-storage)
               (get member :consolidation)
               (get member :shared-member)
               (get member :time-balance)
               (get member :two-pass-calc)
               (get member :variance-reporting)
               (csv/write-csv [(vec (sort (map str (get member :uda))))]
                              :end-of-line "")
               (get member :member-formula)]
              (mapv #(get-in member [:attributes %1] "") attribute-keys))))

(defn relational-extract
  "Prepare and write relational extract to target."
  [{:keys [source target extract alias-table delimiter eol force-quote]
    :as _}]
  (let [mbrs (->> (cached source)
                  (filter (comp (partial = extract) :name))
                  first
                  member-seq
                  (drop 1)
                  vec)]
    (when (empty? mbrs)
      (throw (ex-info "Dimension not present or empty?"
                      {:dimension extract :source source})))
    (let [attrs  (generate-attribute-keys mbrs)
          output (sink (csv {:delimiter delimiter
                             :eol ({"dos" "\r\n" "unix" "\n"} eol)
                             :force-quote force-quote})
                       (if (nil? target) *out* target))]
      (output
       (cons (into ["Parent" "Name" "Alias" "DataStorage" "Consolidation"
                    "SharedMember" "TimeBalance" "TwoPassCalc"
                    "VarianceReporting" "UDAs" "MemberFormula"] attrs)
             (mapv (partial member-row alias-table attrs) mbrs))))))
;; main

(defn run
  "Execute the --extract command - write to file."
  [{:keys [source] :as options}]
  (try
    (when-not source
      (throw (ex-info "Requires valid --source" {:source source})))
    (condp = (:format options)
      "gen"        (gen-extract options)
      "flat"       (flat-extract options)
      "eflat"      (flat-extract options)
      "relational" (relational-extract options)
      (throw (ex-info "Unknown --format." {:format (:format options)})))
    0
    (catch Exception ex
      (println "There were errors:")
      (println ex)
      1)))
